import styled from 'styled-components';

export const DivRodape = styled.div`
    color: #fff;
    background-color: #28282e;
    padding: 100px 0;

    .icone {
        margin-left: 20px;
        margin-right: 20px;
        float: left;
        color: #bbbbbb;
    }

    .conteudo{
        float: left;
        color: #bbbbbb;
    }

    .redes-sociais {
        margin-top: 20px;
    }

    .redes-sociais-icone {
        margin-right: 20px;
        margin-bottom: 30px;

        &:nth-child(1){
            margin-left: 15px;
        }
    }
`;

export const Copyright = styled.div`
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: #131212;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;

    span {
        color: #fff;
    }

    img {
        margin-left: 5px;
        margin-bottom: 5px;
    }
`;