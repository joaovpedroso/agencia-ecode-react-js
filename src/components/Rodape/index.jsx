import React, { useState } from 'react';

import { Container, Row, Col } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbtack, faPhone, faPhoneAlt, faEnvelope } from '@fortawesome/free-solid-svg-icons'

import { DivRodape, Copyright } from './style';

import FormContato from '../Contato';

import LogoAgencia from '../../images/logo.svg';
import facebookLogo from '../../images/social-media/facebook.svg';
import instagramLogo from '../../images/social-media/instagram.svg';
import linkedinLogo from '../../images/social-media/linkedin.svg';
import youtubeLogo from '../../images/social-media/youtube.svg';

function Rodape(){
    return(
        <>
            <DivRodape>
                <Container>
                    <Row>

                        <Col xs='12' sm='6'>
                            <Row>
                                <Col><h2>Nos encontre</h2></Col>
                            </Row>
                            
                            <Row>
                                <div className='icone'>
                                    <FontAwesomeIcon icon={faThumbtack} />
                                </div>
                                <div className='conteudo'>
                                    <p>
                                        Rua Ministro Oliveira Salazar, 5159 - Sala 01 <br />
                                        CEP: 87502-070 - Umuarama | PR
                                    </p>
                                </div>
                            </Row>

                            <Row>
                                <div className='icone'>
                                    <FontAwesomeIcon icon={faPhone} />
                                </div>
                                <div className='conteudo'>
                                    <p>
                                        (44) 3624-3639
                                    </p>
                                </div>
                            </Row>

                            <Row>
                                <div className='icone'>
                                    <FontAwesomeIcon icon={faPhoneAlt} />
                                </div>
                                <div className='conteudo'>
                                    <p>
                                        (44) 99941-5976
                                    </p>
                                </div>
                            </Row>
                        
                            <Row>
                                <div className='icone'>
                                    <FontAwesomeIcon icon={faEnvelope} />
                                </div>
                                <div className='conteudo'>
                                    <p>            
                                        contato@agenciaecode.com.br
                                    </p>
                                </div>
                            </Row>
                        
                            <Row className='redes-sociais'>
                                <Col>
                                    <h2>Redes Sociais</h2>
                                </Col>
                            </Row>

                            <Row>
                                <div className='redes-sociais-icone'>
                                    <img src={facebookLogo} alt='' width='40'/>
                                </div>
                                <div className='redes-sociais-icone'>
                                    <img src={instagramLogo} alt='' width='40'/>
                                </div>
                                <div className='redes-sociais-icone'>
                                    <img src={linkedinLogo} alt='' width='40'/>
                                </div>
                                <div className='redes-sociais-icone'>
                                    <img src={youtubeLogo} alt='' width='40'/>
                                </div>
                            </Row>

                        </Col>


                        <Col xs='12' sm='6'>
                            <Row>
                                <Col><h2>Entre em contato</h2></Col>
                            </Row>
                            <Row>
                                <Col>
                                    <FormContato />
                                </Col>
                            </Row>
                        </Col>


                    </Row>
                </Container>
            </DivRodape>

            <Copyright>
                <span>
                    © 2020 - Todos os direitos reservados | 
                    Desenvolvido por 
                </span>
                <img src={LogoAgencia} alt='Agẽncia e/code' width={65} />
            </Copyright>
        </>
    );
}

export default Rodape;