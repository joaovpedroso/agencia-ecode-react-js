import React, { useState } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLaptopCode, faPencilAlt, faCommentDots } from '@fortawesome/free-solid-svg-icons'

import { SecaoTrabalhos, Item } from './style';

function Trabalhos(){
    return(
        <SecaoTrabalhos>
            <Item>
                <div className='icone'>
                    <FontAwesomeIcon icon={faLaptopCode} />
                </div>

                <h4>Como Trabalhamos?</h4>
                <p>
                    Somos especializados no atendimento <br />
                    on-line à distância. Conectados por e-mail, <br /> 
                    whatsapp, telefone e skype. <br />
                    Fazemos questão de conversar e <br />
                    identificar com cada cliente a melhor <br />
                    solução para sua necessidade.
                </p>
            </Item>

            <Item>
                <div className='icone'>
                    <FontAwesomeIcon icon={faPencilAlt} />
                </div>

                <h4>Por Que Fazemos?</h4>
                <p>
                    Fazemos isso pela paixão por tecnologia <br />
                    e desenvolvimento web. Nosso principal <br />
                    objetivo é melhorar e destacar a sua <br />
                    empresa no meio digital. <br />
                    Venha fazer parte da nossa história!
                </p>
            </Item>

            <Item>
                <div className='icone'>
                    <FontAwesomeIcon icon={faCommentDots} />
                </div>

                <h4>Solicite um orçamento?</h4>
                <p> 
                    Envie sua mensagem para a e/code <br />
                    Agência Digital e conte pra gente qual a <br />
                    sua ideia. Teremos o maior prazer em <br />
                    estar com você nesse mundo digital!                        
                </p>
            </Item>
        </SecaoTrabalhos>
    );
}

export default Trabalhos;