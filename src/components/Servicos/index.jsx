import React, { useState } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
    faLaptopCode, 
    faWindowRestore, 
    faShoppingBasket, 
    faMobileAlt, 
    faSearch,
    
    faProjectDiagram,
    faQuoteRight,
    faEnvelopeOpenText,
    faCloudUploadAlt,
    faPuzzlePiece,
    faNewspaper,
    
} from '@fortawesome/free-solid-svg-icons'


import { SecaoServicos, Item } from './style';

function Servicos(){
    return(
        <SecaoServicos>
            <h2>Serviços</h2>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faLaptopCode} />
                </div>

                <div className='descricao-item'>
                    <h4>Desenvolvimento de Sistemas (Personalizados)</h4>
                    <p>
                        Está precisando de uma solução para os problemas próprios do seu negócio? Pensou em um sistema que atenda as necessidades do seu dia-a-dia? Procura por uma ferramenta que aumente a eficiência no seu trabalho? A e/code desenvolve para você! Fazemos um levantamento das suas necessidades para elaborar uma proposta que as atenda. Desenvolvemos e entregamos soluções que otimizam processos e tornam a sua rotina cada vez mais eficiente.
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faWindowRestore} />
                </div>

                <div className='descricao-item'>
                    <h4>Desenvolvimento de Site (Criação de Sites)</h4>
                    <p>
                        Vai usar a internet para divulgar o seu negócio? A e/code tem tudo o que precisa para fazer o seu site! Tenha um site personalizado com a sua identidade visual, moderno, de acordo com as tendências mais contemporâneas de desenvolvimento web. Nosso processo de criação é dinâmico e rápido.
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faShoppingBasket} />
                </div>

                <div className='descricao-item'>
                    <h4>Loja Virtual (E-commerce)</h4>
                    <p>
                        Quer ampliar o seu negócio? Vender mais, para mais pessoas? Estar ao mesmo tempo em lugares diferentes? Com você pensamos em todo o processo de vendas on-line. Desde o layout atrativo, controle de estoque, cálculo de frete e um suporte adequado. Os profissionais da e/code são especializados para entregar uma ferramenta de vendas fácil de usar, customizável e com excelência na apresentação dos resultados.
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faMobileAlt} />
                </div>

                <div className='descricao-item'>
                    <h4>Aplicativos</h4>
                    <p>
                        Precisa de uma solução para um problema cotidiano? Por que não ter a tecnologia em seu favor e utilizar o smartphone para isso? A e/code desenvolve aplicativos para iOS e Android personalizados, para que a praticidade do celular seja utilizada em seu favor.
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faSearch} />
                </div>

                <div className='descricao-item'>
                    <h4>SEO</h4>
                    <p>
                        Quer estar bem posicionado nos sites de busca? SEO é uma das opções que pode lhe trazer bons resultados. SEO a sigla em inglês para Search Engine Optimization que em português é conhecida como Otimização de Sites. Esse é um trabalho de configuração e organização do seu site para que obtenha os melhores resultados.
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faShoppingBasket} />
                </div>

                <div className='descricao-item'>
                    <h4>Adwords</h4>
                    <p>
                        Conhece o Google? Já usou ele para fazer busca por algum produto ou serviço? Que tal você ser encontrado lá também? Todos os dias centenas de milhares de pessoas utilizam o google para fazer buscas por produtos e serviços e a sua empresa pode estar no topo dessas pesquisas. Com a utilização correta dessa ferramenta sua marca aparece na hora certa para as pessoas certas. Na e/code temos uma equipe preparada para fazer esse trabalho para você!
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faProjectDiagram} />
                </div>

                <div className='descricao-item'>
                    <h4>Google Analytics</h4>
                    <p>
                        Está investindo na divulgação do seu negócio na internet? Precisa de relatórios para saber o que está indo bem e o que pode melhorar? O Analytics é uma ferramenta de relatórios completa, eficiente e fácil de usar. Você tem acesso a informações como quantidade de acessos nas suas publicações/site. Pode personalizar os relatórios por região. Tem acesso a diferentes dados que o auxiliam na tomada de decisão para que suas ações sejam cada vez mais precisas. A e/code pode deixá-la pronta para você usar.
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faQuoteRight} />
                </div>

                <div className='descricao-item'>
                    <h4>Desenvolvimento de Blogs</h4>
                    <p>
                        Precisa interagir mais com seu público? Tem um assunto ou conteúdos que quer disponibilizar para o público com uma linguagem própria? O Blog é um canal de comunicação que o aproxima do público. Você pode fidelizar visitantes, reforçar a sua marca, interagir com as pessoas e tratar de assuntos específicos à sua maneira. A e/code tem ferramentas e profissionais que podem desenvolver um blog com a sua cara para chamar de seu.
                    </p>
                </div>

                </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faMobileAlt} />
                </div>

                <div className='descricao-item'>
                    <h4>Mídias Sociais (Redes Sociais)</h4>
                    <p>
                        Facebook, Instagram, LinkedIn, Google Plus…. Hoje em dia há diferentes canais de comunicação que o aproximam do cliente e contribuem com a divulgação do seu negócio. Seja visto, seja lembrado. Esteja onde seu público está! Venha para a e/code que temos pacotes de trabalho para trazer o seu negócio para a internet!
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faEnvelopeOpenText} />
                </div>

                <div className='descricao-item'>
                    <h4>E-mail Marketing</h4>
                    <p>
                        Quer manter um relacionamento próximo com seus clientes e parceiros? O E-mail Marketing é uma forma de chegar mais perto de quem é importante para você e para o seu negócio. E o melhor de tudo é prático e rápido! A e/code pode desenvolver o conteúdo e dispará-lo para você!
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faCloudUploadAlt} />
                </div>

                <div className='descricao-item'>
                    <h4>Hospedagem</h4>
                    <p>
                        Para que o seu site ou sistema web funcione bem e esteja seguro é importante estar hospedado em um servidor de confiança! A e/code tem parceiros preparados para receber a sua solução e lhe garantir eficiência e segurança.
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faPuzzlePiece} />
                </div>

                <div className='descricao-item'>
                    <h4>Consultoria Web</h4>
                    <p>
                        São muitas ferramentas e soluções que a Web pode oferecer a você e ao seu negócio. Ainda não sabe quais utilizar? Qual caminho seguir? Faça uma consultoria web. Na e/code temos profissionais que vão ouvir e levantar as suas expectativas e necessidades, diante deste mundo de alternativas, lhe fornecer informações e desenvolver um pacote de soluções que o deixem mais próximo do alcance dos seus objetivos.
                    </p>
                </div>

            </Item>

            <Item>

                <div className='icone'>
                    <FontAwesomeIcon icon={faNewspaper} />
                </div>

                <div className='descricao-item'>
                    <h4>Gestão de Conteúdo</h4>
                    <p>
                        Quer colocar sua marca no mundo digital mas não tem tempo? A e/code disponibiliza esse serviço para você. Com pacotes especiais temos profissionais que realizam o trabalho de postar e gerenciar o conteúdo nas suas páginas.                       
                    </p>
                </div>

            </Item>

        </SecaoServicos>
    );
}

export default Servicos;