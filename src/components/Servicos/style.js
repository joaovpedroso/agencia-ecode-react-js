import styled from 'styled-components';

import bgIcone from '../../images/icon-bg.png';

export const SecaoServicos = styled.div`
    padding: 100px 0;
    display: flex;
    flex-direction: row;
    align-items: center;
    flex-wrap: wrap;
    text-align: center;

    h2 {
        font-size: 50px;
        margin-bottom: 30px;
        font-weight: 700;
        flex: 100%;
    }
`;

export const Item = styled.div`
    flex: 50%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;

    .icone{
        width: 124px;
        height: 140px;
        background: url(${bgIcone}) no-repeat;
        background-position: center;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
        flex: 20%;
    }

    .descricao-item {
        flex: 60%;
        text-align: left;
        padding: 20px 50px 40px 0;

        h4 {
            font-weight: 700;
            font-size: 20px;
            margin-bottom: 20px;
        }

        p {
            font-size: 15px;
        }
    }

    svg {
        font-size: 50px;
        color: #51ae32;
    }

`;