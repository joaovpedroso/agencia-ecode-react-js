import React, { useState, useEffect } from 'react';

import { Row, Col } from 'reactstrap';

import { SecaoPortifolio, PortfolioFiltros, PortfolioContent, Item } from './style';

function Portfolio(){

    const [filtro, setFiltro] = useState('');

    useEffect( () => {
        setFiltro('todos');

    }, []);


    useEffect(() => {
        const itens = Array.from(document.getElementsByClassName('portfolio-itens'));
        
        itens.map((item, key, itens) => {
            if( itens[key].classList.contains(filtro) ) {
                itens[key].classList.remove('hide');
                itens[key].classList.add('show');
            } else {
                itens[key].classList.remove('show');
                itens[key].classList.add('hide');
            }

        });

    }, [filtro]);

    return(
        <SecaoPortifolio>

            <h2>Portfólio</h2>

            <PortfolioFiltros>
                <div className='filtros'>
                    <button 
                        className={ filtro == 'todos' ? 'ativo' : '' }
                        data-filtro='todos' 
                        onClick={() => setFiltro('todos') }
                    >
                        Todos
                    </button>
                    <button 
                        className={ filtro == 'sites' ? 'ativo' : '' }
                        data-filtro='sites' 
                        onClick={() => setFiltro('sites') }
                    >
                        SITES
                    </button>
                    <button 
                        className={ filtro == 'ecommerce' ? 'ativo' : '' }
                        data-filtro='ecommerce' 
                        onClick={() => setFiltro('ecommerce') }
                    >
                        E-COMMERCE
                    </button>
                    <button 
                        className={ filtro == 'aplicativos' ? 'ativo' : '' }
                        data-filtro='aplicativos' 
                        onClick={() => setFiltro('aplicativos') }
                    >
                        APLICATIVOS
                    </button>
                    <button 
                        className={ filtro == 'midias-sociais' ? 'ativo' : '' }
                        data-filtro='midias-sociais' 
                        onClick={() => setFiltro('midias-sociais') }
                    >
                        MÍDIAS SOCIAIS
                    </button>
                </div>                
            </PortfolioFiltros>

            <PortfolioContent>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                            <img 
                                src='https://www.agenciaecode.com.br/portfolio-new/site-maria-macia.jpg' 
                                alt='Maria Macia'
                            />
                        </div>
                        <h3>Maria Macia</h3>
                        <span>Website e Área Administrativa</span>
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos midias-sociais'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                            <img 
                                src='https://www.agenciaecode.com.br/portfolio-new/midias-azmax.jpg'
                                alt='AZmax Colchões'
                            />
                        </div>
                        <h3>AZmax Colchões</h3>
                        <span>Gestão de Conteúdo para Mídias Sociais</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos midias-sociais'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                            <img 
                                src="https://www.agenciaecode.com.br/portfolio-new/midias-yazigi.jpg" 
                                alt="Yázig" 
                                title="Yázig" />
                        </div>
                        <h3>Yázigi - Escola de Idiomas</h3>
                        <span>Gestão de Conteúdo para Mídias Sociais</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos midias-sociais'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/midias-dinprest.jpg" 
                            alt="Dinprest" 
                            title="Dinprest"
                        />
                        </div>
                        <h3>Dinprest Empréstimos</h3>
                        <span>Gestão de Conteúdo para Mídias Sociais</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos aplicativos'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/app-jeitogazin.jpg" 
                            alt="Jeito Gazin" 
                            title="Jeito Gazin"
                        />
                        </div>
                        <h3>Jeito Gazin</h3>
                        <span>Aplicativo</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos aplicativos'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/app-consorciogazin.jpg" 
                            alt="Consórcio Gazin" 
                            title="Consórcio Gazin"
                        />
                        </div>
                        <h3>Consórcio Gazin</h3>
                        <span>Aplicativo</span> 
                    </a>                    
                </Item>
               
                <Item className='portfolio-itens todos aplicativos'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/app-gazinatacado.jpg" 
                            alt="Gazin Atacado" 
                            title="Gazin Atacado"
                        />
                        </div>
                        <h3>Gazin Atacado</h3>
                        <span>Aplicativo</span>  
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-norospar.jpg" 
                            alt="Norospar" 
                            title="Norospar"
                        />
                        </div>
                        <h3>Norospar</h3>
                        <span>Website e Área Administrativa</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-cwr.jpg" 
                            alt="CWR Contábil" 
                            title="CWR Contábil"
                        />
                        </div>
                        <h3>CWR Contábil</h3>
                        <span>Website</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-iqueuti.jpg" 
                            alt="Iqueuti Negócios Imobiliários" 
                            title="Iqueuti Negócios Imobiliários"
                        />
                        </div>
                        <h3>Iqueuti Negócios Imobiliários</h3>
                        <span>Website e Área Administrativa</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-consorcio.jpg" 
                            alt="Consórcio Gazin" 
                            title="Consórcio Gazin"
                        />
                        </div>
                        <h3>Consórcio Gazin</h3>
                        <span>Website e Área Administrativa</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-expo.jpg" 
                            alt="Expo Umuarama" 
                            title="Expo Umuarama"
                        />
                        </div>
                        <h3>Expo Umuarama</h3>
                        <span>Website e Área Administrativa</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-capital.jpg" 
                            alt="Capital Filtros e Lubrificantes" 
                            title="Capital Filtros e Lubrificantes"
                        />
                        </div>
                        <h3>Capital Filtros e Lubrificantes</h3>
                        <span>Website e Área Administrativa</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-famas.jpg" 
                            alt="Cadeiras Fama" 
                            title="Cadeiras Fama"
                        />
                        </div>
                        <h3>Cadeiras Fama</h3>
                        <span>Website e Área Administrativa</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-kids.jpg" 
                            alt="Revista Kids Mais" 
                            title="Revista Kids Mais"
                        />
                        </div>
                        <h3>Revista Kids Mais</h3>
                        <span>Website e Área Administrativa</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-quality.jpg" 
                            alt="QualityNet" 
                            title="QualityNet"
                        />
                        </div>
                        <h3>QualityNet</h3>
                        <span>Website e Área Administrativa</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos ecommerce sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-cotramaq.jpg" 
                            alt="Cotramaq" 
                            title="Cotramaq"
                        />
                        </div>
                        <h3>Cotramaq</h3>
                        <span>Loja Virtual e Identidade Visual</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-seguros.jpg" 
                            alt="Gazin Seguros" 
                            title="Gazin Seguros"
                        />
                        </div>
                        <h3>Gazin Seguros</h3>
                        <span>Website</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-marra.jpg" 
                            alt="Marra Soares Advocacia" 
                            title="Marra Soares Advocacia"
                        />
                        </div>
                        <h3>Marra Soares Advocacia</h3>
                        <span>Website</span> 
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-sorte.jpg" 
                            alt="Consórcio Gazin - Sorte em Dia" 
                            title="Consórcio Gazin - Sorte em Dia"
                        />
                        </div>
                        <h3>Consórcio Gazin - Sorte em Dia</h3>
                        <span>Hotsite e Área Administrativa</span>  
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos ecommerce sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-lista.jpg" 
                            alt="Gazin - Lista de Casamento" 
                            title="Gazin - Lista de Casamento"
                        />
                        </div>
                        <h3>Gazin - Lista de Casamento</h3>
                         <span>Website e Loja</span>
                    </a>                    
                </Item>

                <Item className='portfolio-itens todos sites'>
                    <a href='#'>
                        <div className='imagem-zoom'>
                        <img 
                            src="https://www.agenciaecode.com.br/portfolio-new/site-inga.jpg" 
                            alt="Ingazin - Comitê de Inovações" 
                            title="Ingazin - Comitê de Inovações"
                        />
                        </div>
                        <h3>Ingazin - Gazin</h3>
                        <span>Website</span>
                    </a>                    
                </Item>


            </PortfolioContent>


        </SecaoPortifolio>
    );
}

export default Portfolio;