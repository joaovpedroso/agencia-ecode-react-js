import styled from 'styled-components';

export const SecaoPortifolio = styled.div`
    padding: 100px 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    h2 {
        font-size: 50px;
        margin-bottom: 30px;
        font-weight: 700;
    }

    a {
        color: #fff;
        background-color: #5cb85c;
        border-color: #4cae4c;
        padding: 6px 12px;
        margin-top: 20px;
        text-decoration: none;
        outline: none;

        &:hover{
            outline: none;
            background-color: #449d44;
            border-color: #398439;
        }
    }
`;

export const PortfolioFiltros = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 20px;
    
    .filtros {
        width: auto;
        background: #fff;
        border-radius: 30px;
        box-shadow: 0 0 20px 0 #dddcd9;

        button {
            width: auto;
            text-align: center;
            padding: 13px 36px;
            color: #20202f;
            font-weight: 600;
            text-decoration: none;
            border: 0;
            background: transparent;
            font-size: 14px;
            border-radius: 30px;
            outline: none;

            &:hover {
                transition: all 0.5s ease 0s;
                color: #52ae32;
                outline: none;
            }
        }

        .ativo {
            background: #20202f;
            color: #ffffff;

            &:hover {
                transition: all 0.5s ease 0s;
                color: #ffffff;
            }
        }

    }

    

`;

export const PortfolioContent = styled.div`
    width: 100%;
    margin: 0;
    padding: 0 40px 0 40px;
    border-left: 1px solid #fff;
    overflow: hidden;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;

    .hide { display: none; }
    .show { display: block; }
`;

export const Item = styled.div`
    height: 380px;
    width: 300px;
    margin: 0 20px 0 20px;
    text-align: center;

    a {
        text-decoration: none;
        outline: none;
        background: transparent;
        border: 0;

        .imagem-zoom {
            height: 200px;
            overflow: hidden;
            box-shadow: 0 0 17px 0 #dddcd9;

            img {
                width: 100%;
                transition: transform .5s ease;
            }

            &:hover {
                img {
                    transform: scale(1.2);
                }
            }
        }

        h3 {
            margin-top: 10px;
            font-size: 22px;
            font-weight: 700;
            margin-bottom: 2px;
            color: #424242;
        }

        span {
            color: #424242;
            outline: 0;
            font-size: 14px;
        }

        span,
        h3 {
            &:hover{
                transition: all 0.5s ease 0s;
                color: #52ae32;
            }
        }

        &:hover {
            text-decoration: none;
            outline: none;
            background: transparent;
            border: 0;
        }
    }

`;