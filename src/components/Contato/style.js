import styled from 'styled-components';

export const FormContato = styled.form`
    width: 100%;
    
    .form-group {

        input {
            width: 100%;
            height: 40px;
            border: none;
            border-bottom: 1px solid #a2a2a2;
            background: 0 0;
            margin-bottom: 15px;
            color: #fff;
        }

        textarea {
            color: #fff;
            width: 100%;
            height: 100px;
            border: none;
            border-bottom: 1px solid #a2a2a2;
            background: 0 0;
        }

    }

    button {
        margin-top: 10px;
        background: 0 0;
        border: 1px solid #666;
        color: #fff;
        font-weight: 700;
        padding: 6px 20px;
    }
`;