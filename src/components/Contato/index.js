import React from 'react';

import { FormContato } from './style.js';

function ContatoForm(){

    return(
        <FormContato>
            
            <div className='form-group'>
                <input type='text' name='nome' value='' placeholder='Nome' size='30' required onChange={() => console.log(this)} />
            </div>

            <div className='form-group'>
                <input type='email' name='email' value='' placeholder='Email' size='30' required onChange={() => console.log(this)} />
            </div>

            <div className='form-group'>
                <input type='text' name='assunto' value='' placeholder='Assunto' size='30' required onChange={() => console.log(this)} />
            </div>

            <div className='form-group'>
                <textarea name='mensagem' placeholder='Mensagem' required onChange={() => console.log(this)} ></textarea>
            </div>

            <button type='submit'>
                Enviar
            </button>

        </FormContato>
    );

}

export default ContatoForm;