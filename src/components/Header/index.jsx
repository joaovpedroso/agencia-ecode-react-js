import React, { useState } from 'react';

import Navbar from '../Navbar';
import Banner from '../Banner';

import { FullPageHeader } from './styles';

function Header(){
    return(
        <FullPageHeader>
            <Navbar />
            <Banner strings={[
                'Sempre Criativos',
                'Sempre Evoluindo',
                'Sempre Inovando',
                'Sempre Confiável',
            ]}/>
        </FullPageHeader>
    );
}

export default Header;