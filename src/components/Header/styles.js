import styled from 'styled-components';

import bgBanner from '../../images/bg-home-fullscreen.jpg';

export const FullPageHeader = styled.div`
    width: 100%;
    height: 900px;
    background-image: url(${bgBanner});
    background-size: cover;
    backgrond-position: center;
    background-repeat: no-repeat;
    box-sizing: border-box;
    overflow: hidden;
`;