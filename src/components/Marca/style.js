import styled from 'styled-components';

export const SecaoMarca = styled.div`
    padding: 100px 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    .gifAgencia {
        max-width: 450px;
        margin: 25px 0;
    }

    p {
        margin-bottom: 20px;
        font-size: 17px;
        text-align: center;
    }
`;