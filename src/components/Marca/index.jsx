import React, { useState } from 'react';

import { SecaoMarca } from './style';

import gifAgencia from '../../images/ecode_animate.gif';
import logoBarra from '../../images/logo-barra.jpg';
import logoTag from '../../images/logo-tag.jpg';
import logoChave from '../../images/logo-chave.jpg';
import logoe from '../../images/logo-e.jpg';

function Marca(){
    return(
        <SecaoMarca>
            <img src={gifAgencia} alt='Agência e/code' className='gifAgencia' />

            <p>
                Uma marca dinâmica e flexível, como nosso trabalho. Code vem do inglês código, por isso você poderá nos ver com o&nbsp;
                <img src={logoBarra} alt='Agência e/code' /> ,&nbsp;
                <img src={logoTag} alt='Agência e/code' /> ou&nbsp;
                <img src={logoChave} alt='Agência e/code' />&nbsp; <br />
                elementos da linguagem que usamos para programar e assim entregar resultados aos nossos clientes.
            </p>

            <p>
                Quando começamos a proposta era trabalhar com e-commerces foi então que o
                &nbsp;<img src={logoe} alt='Agência e/code' />&nbsp;
                entrou em cena. É uma referência ao eletrônico, que agora <br/>
                aprimoramos para o digital.
            </p>

        </SecaoMarca>
    );
}

export default Marca;