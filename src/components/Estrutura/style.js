import styled from 'styled-components';

import bgEstrutura from '../../images/bg-1.jpg';
import bordaIcone from '../../images/icon-border.png';
import bgIcone from '../../images/icon-bg-white.png';

export const SecaoEstrutura = styled.div`
    background: url(${bgEstrutura})no-repeat;
    background-size: cover;
    background-position: center;
    padding: 100px 0;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    text-align: center;

    h2 {
        font-size: 50px;
        margin-bottom: 30px;
        font-weight: 700;
        color: #fff;
        flex: 100%;
    }

`;

export const Item = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 20px 60px 20px 60px;
    color: #fff;

    .icone{
        width: 124px;
        height: 140px;
        background: url(${bordaIcone}) no-repeat;
        background-position: center;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    svg {
        font-size: 50px;
        color: #fff;
    }

    h4 {
        margin-top: 20px;
        margin-bottom: 10px;
        font-weight: 600;
        font-size: 18px;
        text-transform: uppercase;
    }

    p {
        line-height: 24px;
        margin-bottom: 20px;
        font-size: 17px;
        text-align: center;
    }

    &:hover{
        .icone {
            background: url(${bgIcone}) no-repeat;
        }

        svg {
            color: #000;
        }
    }
`;