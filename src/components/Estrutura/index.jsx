import React, { useState } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChartLine, faCogs, faCloudUploadAlt } from '@fortawesome/free-solid-svg-icons'

import { SecaoEstrutura, Item } from './style';

function Estrutura(){
    return(
        <SecaoEstrutura>
            <h2>O que nós fazemos?</h2>
            
            <Item>
                <div className='icone'>
                    <FontAwesomeIcon icon={faChartLine} />
                </div>

                <h4>01 - Planejamento</h4>
                <p>
                    Pesquisar, buscar alternativas, traçar <br />
                    objetivos é fundamental para obter <br />
                    sucesso no desenvolvimento de todo <br />
                    projeto. Por isso nosso trabalho sempre <br />
                    começa com o planejamento.
                </p>
            </Item>

            <Item>
                <div className='icone'>
                    <FontAwesomeIcon icon={faCogs} />
                </div>

                <h4>02 - Desenvolvimento</h4>
                <p>
                    Essa é a nossa força. Desenvolvemos <br />
                    soluções e monitoramos o trajeto para <br />
                    garantir a entrega de resultados efetivos.
                </p>
            </Item>

            <Item>
                <div className='icone'>
                    <FontAwesomeIcon icon={faCloudUploadAlt} />
                </div>

                <h4>03 - Lançamento</h4>
                <p>
                    Trabalho feito é hora de fazer acontecer! <br />
                    Testamos juntos e fazemos ajustes para <br />
                    garantir que o objetivo definido seja <br />
                    alcançado.
                </p>
            </Item>

        </SecaoEstrutura>
    );
}

export default Estrutura;