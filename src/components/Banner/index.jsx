import React, { useState ,useEffect } from 'react';

import { Header, TypedComponent, Linha } from './style';

import Typed from 'typed.js';

const Banner = (props) => {

    const [typedComponent, setTypedComponent] = useState('');

    useEffect(() => {
        
        const { strings } = props;       
        const options = ({
            strings,
            typeSpeed: 100,
            backSpeed: 50,
            backDelay: 5000,
            smartBackspace: true,
            loop: true
        });

        setTypedComponent(new Typed('.typedComponent', options));

    }, [])



    return(
        <Header>
            <TypedComponent>
                <div className='typedComponent'></div>
            </TypedComponent>

            <Linha />

            <h3>Fazemos a web ≠ diferente, todos os dias.</h3>

       </Header>
    );
}

export default Banner;