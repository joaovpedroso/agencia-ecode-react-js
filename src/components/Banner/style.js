import styled from 'styled-components';

export const Header = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    box-sizing: border-box;

    h3 {
        font-size: 20px;
        font-weight: 300;
        line-height: 30px;
        color: #fff;
        margin-top: 20px;
    }
`;

export const TypedComponent = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    
    .typedComponent{
        font-size: 70px;
        font-weight: 700;
        color: #fff;
        margin-bottom: 10px;
    }

    .typed-cursor{
        background: #fff;
        color: #fff;
        margin-left: 20px;
        height: 50px;
        width: 3px;
    }

    @media (max-width: 768px){
        .typedComponent{
            font-size: 30px;
        }
        .typed-cursor{
            margin-left: 10px;
            height: 30px;
        }
    }
`;

export const Linha = styled.div`
    width: 90px;
    height: 3px;
    background: #51ae32;
`;