import styled from 'styled-components';

export const ToggleButton = styled.div`
    cursor: pointer;

    @media (min-width: 769px) {
        display: none;
    }
`;
export const NavbarColor = styled.div`
    ul {

        li {

            a {
                color: #fff;
                font-size: 15px;
                font-weight: 500;
                line-height: normal;
                border-bottom: 3px solid transparent;

                &:hover{
                    border-bottom: 3px solid #51ae32;
                }
            }

        }

    }
`;

export const NavDesktop = styled.div`
    @media (max-width: 768px) {
        display: none;
    }
`;