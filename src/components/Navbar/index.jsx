import React, { useState } from 'react';

import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import NavItens from './nav-itens';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'

import { NavDesktop, ToggleButton } from './style';

import LogoAgencia from '../../images/logo.svg';

function Navigationbar(){
    
    const [collapsed, setCollapsed] = useState(true);
    const toggleNavbar = () => setCollapsed(!collapsed);

    return(
        <Navbar fixed='top'>
            <NavbarBrand href="/" className="mr-auto">
                <img src={LogoAgencia} alt='Agência e/Code' className='img-fluid' width={160}/>
            </NavbarBrand>

            <ToggleButton onClick={toggleNavbar} className="mr-2">
                <FontAwesomeIcon icon={faBars} color='#eee' />
            </ToggleButton>
            
            <NavDesktop>
                <NavItens />
            </NavDesktop>

            <Collapse isOpen={!collapsed} navbar>
                <NavItens />
            </Collapse>
        </Navbar>
    );


}

export default Navigationbar;