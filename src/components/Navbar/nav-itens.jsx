import React, { useState } from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';

import { NavbarColor } from './style';

function NavItens(){
    return(
        <NavbarColor>
            <Nav>
                <NavItem>
                    <NavLink href="/">Home</NavLink>
                </NavItem>
                
                <NavItem>
                    <NavLink href="/">Sobre</NavLink>
                </NavItem>

                <NavItem>
                    <NavLink href="/">O que nós fazemos?</NavLink>
                </NavItem>

                <NavItem>
                    <NavLink href="/">Serviços</NavLink>
                </NavItem>

                <NavItem>
                    <NavLink href="/">Portfólio</NavLink>
                </NavItem>

                <NavItem>
                    <NavLink href="/blog">Blog</NavLink>
                </NavItem>

                <NavItem>
                    <NavLink href="/">Trabalhe Conosco</NavLink>
                </NavItem>

                <NavItem>
                    <NavLink href="/">Contato</NavLink>
                </NavItem>
            </Nav>
        </NavbarColor>
    );


}

export default NavItens;