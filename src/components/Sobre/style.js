import styled from 'styled-components';

export const SecaoSobre = styled.div`
    padding: 100px 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    h2 {
        font-size: 50px;
        margin-bottom: 30px;
        font-weight: 700;
    }

    p {
        text-align: center;
        line-height: 28px;
        font-size: 18px;
    }

    a {
        color: #fff;
        background-color: #5cb85c;
        border-color: #4cae4c;
        padding: 6px 12px;
        margin-top: 20px;
        text-decoration: none;
        outline: none;

        &:hover{
            outline: none;
            background-color: #449d44;
            border-color: #398439;
        }
    }
`;