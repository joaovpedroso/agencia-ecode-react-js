import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { SecaoSobre } from './style';

function Sobre(){
    return(
        <SecaoSobre>
            <h2>Sobre</h2>
            <p>
                A <strong>e/code</strong> é <strong>uma Agência Digital</strong>, que vem para surpreendê-lo com <br />
                qualidade e exclusividade em todos os projetos.
            </p>

            <p>
                Trabalhamos com idéias originais com o objetivo de transformá-las em um <br />
                resultado inovador e mensurável.
            </p>

            <Link to='#'>
                Por que e/code?
            </Link>

        </SecaoSobre>
    );
}

export default Sobre;