import React, { useState } from 'react';

import Header from '../components/Header';
import Sobre from '../components/Sobre';
import Trabalhos from '../components/Trabalhos';
import Marca from '../components/Marca';
import Estrutura from '../components/Estrutura';
import Servicos from '../components/Servicos';
import Portfolio from '../components/Portfolio';
import Rodape from '../components/Rodape';

function Home(){

    return(
        <>
            <Header />
            <Sobre />
            <Trabalhos />
            <Marca />
            <Estrutura />
            <Servicos />
            <Portfolio />
            <Rodape />
        </>
    );

}

export default Home;